package com.jimo.synchronization;

import java.util.List;

/**
 * Created by ThinkPad on 2018/8/9.
 */
public class SynchronizedCounter {
    private int counter;
    private List<String> nameList;

    public synchronized void increment() {
        counter++;
    }

    public synchronized void decrement() {
        counter--;
    }

    public synchronized int getValue() {
        return counter;
    }

    int nameCount;

    public void addName(String name) {
        synchronized (this) {
            String lastName = name;
            nameCount++;
        }
        nameList.add(name);
    }
}
