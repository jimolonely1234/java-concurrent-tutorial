package com.jimo.synchronization;

/**
 * Created by ThinkPad on 2018/8/9.
 */
public class MsLunch {
    private int c1;
    private int c2;
    private Object lock1 = new Object();
    private Object lock2 = new Object();

    public void inc1() {
        synchronized (lock1) {
            c1++;
        }
    }

    public void inc2() {
        synchronized (lock2) {
            c2++;
        }
    }
}
