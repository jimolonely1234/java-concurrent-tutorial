package com.jimo.synchronization;

/**
 * Created by ThinkPad on 2018/8/9.
 */
public class Counter {
    private int counter = 0;

    public void increment() {
        counter++;
    }

    public void decrement() {
        counter--;
    }

    public int getValue() {
        System.out.println(counter);
        return counter;
    }
}
