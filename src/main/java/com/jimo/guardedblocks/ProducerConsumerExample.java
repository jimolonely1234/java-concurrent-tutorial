package com.jimo.guardedblocks;

/**
 * Created by ThinkPad on 2018/8/9.
 */
public class ProducerConsumerExample {
    public static void main(String[] args) {
        Drop drop = new Drop();
        new Thread(new Producer(drop)).start();
        new Thread(new Consumer(drop)).start();
    }
}
