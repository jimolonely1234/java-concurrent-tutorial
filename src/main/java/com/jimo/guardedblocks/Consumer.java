package com.jimo.guardedblocks;

import java.util.Random;

/**
 * Created by ThinkPad on 2018/8/9.
 */
public class Consumer implements Runnable {
    private Drop drop;

    public Consumer(Drop drop) {
        this.drop = drop;
    }

    public void run() {
        Random random = new Random();
        for (String msg = drop.take(); !"DONE".equals(msg); msg = drop.take()) {
            System.out.println("收到消息：" + msg);
            try {
                Thread.sleep(random.nextInt(5000));
            } catch (InterruptedException e) {
                System.out.println("消费者被中断");
            }
        }
    }
}
