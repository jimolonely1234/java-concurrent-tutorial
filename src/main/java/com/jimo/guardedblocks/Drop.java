package com.jimo.guardedblocks;

/**
 * Created by ThinkPad on 2018/8/9.
 */
public class Drop {
    private String message;
    private boolean empty = true;//当消费者等待生产者时为true

    public synchronized String take() {
        while (empty) {
            try {
                wait();
            } catch (InterruptedException e) {
                System.out.println("消费者等待被中断");
            }
        }
        empty = true; //改变状态，通知生产者生产
        notifyAll();
        return message;
    }

    public synchronized void put(String message) {
        while (!empty) {
            try {
                wait();
            } catch (InterruptedException e) {
                System.out.println("生产者等待被中断");
            }
        }
        empty = false;//生产了就不为空了
        this.message = message;
        notifyAll();
    }
}
