package com.jimo.guardedblocks;

/**
 * Created by ThinkPad on 2018/8/9.
 */
public class GuardJoy {

    private boolean joy;

    public void guardJoy() {
        while (!joy) {
        } //这样做会一直消耗CPU，不可取
        System.out.println("终于享受到Joy了！");
    }

    public synchronized void guardJoy2() {
        /*此方式在每次事件触发时只调用一次，但可能不是我们等待的事件*/
        while (!joy) {
            try {
                wait();
            } catch (InterruptedException e) {
            }
        }
        System.out.println("终于享受到Joy了！");
    }

    public synchronized void notifyJoy() {
        joy = true;
        notifyAll();
    }
}
