package com.jimo.guardedblocks;

import java.util.Random;

/**
 * Created by ThinkPad on 2018/8/9.
 */
public class Producer implements Runnable {
    private Drop drop;

    public Producer(Drop drop) {
        this.drop = drop;
    }

    public void run() {
        String[] infos = {"heheheh", "llllllll", "xxxxxxx", "rrrrrrrr"};
        Random random = new Random();
        for (String s : infos) {
            drop.put(s);
            System.out.println("发送消息：" + s);
            try {
                Thread.sleep(random.nextInt(5000));
            } catch (InterruptedException e) {
                System.out.println("Producer 中断");
            }
        }
        drop.put("DONE");
    }
}
