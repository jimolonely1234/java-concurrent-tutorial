package com.jimo.lock;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * <p>
 * 读写锁分为读锁和写锁，简单来说，读锁可以被多个线程同时持有，而写锁一次只能
 * 被一个线程持有
 * </p>
 * <p>
 * 下面使用ReentrantReadWriteLock（可重入读写锁）演示一个多线程读写字符的
 * 例子。会发现，写锁会被竞争，读线程会等待。
 * </p>
 *
 * @author jimo
 * @date 2018/9/30 8:10
 */
public class ReadWriteLockDemo {
	private static final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
	private static String msg = "";

	public static void main(String[] args) throws InterruptedException {
		final Thread r1 = new Thread(new Reader(), "Reader1");
		final Thread r2 = new Thread(new Reader(), "Reader2");

		final Thread w1 = new Thread(new Writer("a"), "Writer1");
		final Thread w2 = new Thread(new Writer("b"), "Writer2");
		final Thread w3 = new Thread(new Writer("c"), "Writer3");

		r1.start();
		r2.start();
		w1.start();
		w2.start();
		w3.start();

		r1.join();
		r2.join();
		w1.join();
		w2.join();
		w3.join();
	}

	private static class Reader implements Runnable {
		@Override
		public void run() {
			final String name = Thread.currentThread().getName();
			for (int i = 0; i < 3; i++) {
				if (lock.isWriteLocked()) {
					System.out.println(name + " : 读线程尝试获取锁，但被写线程持有");
				}
				lock.readLock().lock();
				System.out.println(name + " : 读线程持有锁");

				try {
					final int duration = ThreadLocalRandom.current().nextInt(1000);
					System.out.println(name + " : 睡眠" + duration + "ms");
					Thread.sleep(duration);
				} catch (InterruptedException e) {
					e.printStackTrace();
				} finally {
					System.out.println(name + " : msg= " + msg);
					lock.readLock().unlock();
				}
			}
		}
	}

	private static class Writer implements Runnable {
		private final String charToWrite;

		private Writer(String charToWrite) {
			this.charToWrite = charToWrite;
		}

		@Override
		public void run() {
			final String name = Thread.currentThread().getName();

			for (int i = 0; i < 3; i++) {
				//直接尝试获取锁
				lock.writeLock().lock();
				System.out.println(name + " : 写线程持有锁");

				try {
					final int duration = ThreadLocalRandom.current().nextInt(1000);
					System.out.println(name + " : 睡眠" + duration + "ms");
					Thread.sleep(duration);
				} catch (InterruptedException e) {
					e.printStackTrace();
				} finally {
					msg = msg.concat(charToWrite);
					System.out.println(name + " : 写入字符 " + charToWrite);
					lock.writeLock().unlock();
				}
			}
		}
	}
}
