package com.jimo.lock;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author jimo
 * @date 2018/10/6 9:26
 */
public class AtomicDemo {
	private static class Counter {
		private AtomicInteger c = new AtomicInteger(0);

		public void incr() {
			c.incrementAndGet();
		}

		public int getValue() {
			return c.get();
		}
	}

	public static void main(String[] args) throws InterruptedException {
		final Counter counter = new Counter();

		for (int i = 0; i < 1000; i++) {
			new Thread(() -> counter.incr()).start();
		}

		Thread.sleep(3000);
		System.out.println("结果为：" + counter.getValue());
	}
}
