package com.jimo.lock;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * <p>
 * ReentrantLock 可重入锁，锁和同步比起来有以下不同：
 * 1.保证序列：同步不能保证序列执行，除非锁整个方法
 * 2.超时机制：同步也没有
 * 3.单一方法：同步只能用于一个方法，而lock可以在不同的方法中lock和unlock
 *
 * <p>
 * 下面使用打印机的例子，一个打印任务必须执行完才能进行下一个，我们使用锁来保证顺序
 * </p>
 *
 * @author jimo
 * @date 2018/9/29 14:26
 */
public class LockDemo {
	/**
	 * 打印机
	 *
	 * @author jimo
	 * @date 2018/9/29 14:33
	 */
	private static class Printer {
		/// lock

		private final Lock lock = new ReentrantLock();

		public void print() {
			lock.lock();
			try {
				final int duration = ThreadLocalRandom.current().nextInt(1000);
				System.out.println(Thread.currentThread().getName() + ": 打印花费时间：" + duration + " ms");
				Thread.sleep(duration);
			} catch (InterruptedException e) {
			} finally {
				System.out.println(Thread.currentThread().getName() + ": 打印成功！");
				lock.unlock();
			}
		}

		/**
		 * 没有锁时
		 *//*
		public void print() {
			try {
				final int duration = ThreadLocalRandom.current().nextInt(1000);
				System.out.println(Thread.currentThread().getName() + ": 打印花费时间：" + duration + " ms");
				Thread.sleep(duration);
				System.out.println(Thread.currentThread().getName() + ": 打印成功！");
			} catch (InterruptedException e) {
			}
		}*/
	}

	private static class Task extends Thread {
		private Printer printer;

		public Task(String name, Printer printer) {
			super(name);
			this.printer = printer;
		}

		@Override
		public void run() {
			System.out.println(Thread.currentThread().getName() + ": 开始打印...");
			printer.print();
		}
	}

	public static void main(String[] args) {
		final Printer printer = new Printer();

		new Task("task 1", printer).start();
		new Task("task 2", printer).start();
		new Task("task 3", printer).start();
		new Task("task 4", printer).start();
	}
}
