package com.jimo.lock;

/**
 * 原子变量测试
 *
 * @author jimo
 * @date 2018/10/6 9:19
 */
public class AtomicTest {
	private static class Counter {
		private int c;

		public void incr() {
			c++;
		}

		public int getValue() {
			return c;
		}
	}

	public static void main(String[] args) throws InterruptedException {
		final Counter counter = new Counter();

		for (int i = 0; i < 1000; i++) {
			final Thread t = new Thread(() -> counter.incr());
			t.start();
		}
		Thread.sleep(3000);
		System.out.println("结果为：" + counter.getValue());
	}
}
