package com.jimo.lock;

/**
 * 演示死锁的由来：因为线程加锁的顺序不一致导致，相互等待对方持有的锁，形成死锁
 *
 * @author jimo
 * @date 2018/9/29 13:46
 */
public class DeadLockDemo {
	private final static Object LOCK1 = new Object();
	private final static Object LOCK2 = new Object();

	public static void main(String[] args) {
		new ThreadDemo1().start();
		new ThreadDemo2().start();
	}

	private static class ThreadDemo1 extends Thread {
		@Override
		public void run() {
			synchronized (LOCK1) {
				System.out.println("线程1持有lock1...");

				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
				}
				System.out.println("线程1等待lock2");

				synchronized (LOCK2) {
					System.out.println("线程1持有lock1和lock2");
				}
			}
		}
	}

	private static class ThreadDemo2 extends Thread {
		@Override
		public void run() {
			synchronized (LOCK1) {
				System.out.println("线程2持有lock1...");

				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
				}
				System.out.println("线程2等待lock2");

				synchronized (LOCK2) {
					System.out.println("线程2持有lock1和lock2");
				}
			}
		}
	}
}
