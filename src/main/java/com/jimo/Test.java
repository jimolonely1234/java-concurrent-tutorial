package com.jimo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author jimo
 * @date 2018/9/26 16:32
 * @code 测试
 */
public class Test {
	public static void main(String[] args) {
		///说明
//		List<String> list = new ArrayList<>();
//		list.add("jimo");
//		list.add("hehe");
//		final List<String> subList = list.subList(0, 1);
//		subList.add("xxx");
//		System.out.println(Arrays.toString(list.toArray()));
//
//		final String[] strs = {"jjj", "xxx"};
//		final List<String> strings = Arrays.asList(strs);
////		strings.add("kkk");

		List<String> list = new ArrayList<>();
		list.add("1");
		list.add("2");
		for (String s : list) {
			if ("2".equalsIgnoreCase(s)) {
				list.remove(s);
			}
		}
		System.out.println(Arrays.toString(list.toArray()));

	}
}
