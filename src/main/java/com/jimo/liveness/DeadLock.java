package com.jimo.liveness;

/**
 * Created by ThinkPad on 2018/8/9.
 */
public class DeadLock {
    static class Friend {
        private String name;

        public Friend(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public synchronized void bow(Friend bower) {
            System.out.format("%s:%s 向我鞠躬！%n", name, bower.getName());
            bower.bowBack(this);
        }

        public synchronized void bowBack(Friend bower) {
            System.out.format("%s:%s 向我回礼鞠躬！%n", name, bower.getName());
        }
    }

    public static void main(String[] args) {
        final Friend jimo = new Friend("jimo");
        final Friend hehe = new Friend("hehe");
        new Thread(new Runnable() {
            public void run() {
                jimo.bow(hehe);
            }
        }).start();
        new Thread(new Runnable() {
            public void run() {
                hehe.bow(jimo);
            }
        }).start();
    }
}
