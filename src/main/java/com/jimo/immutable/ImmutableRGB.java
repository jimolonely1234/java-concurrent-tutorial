package com.jimo.immutable;

public class ImmutableRGB {
    final private int red;
    final private int blue;
    final private int green;
    final private String name;

    private void check(int red, int green, int blue) {
        if (red < 0 || red > 255
                || green < 0 || green > 255
                || blue < 0 || blue > 255) {
            throw new IllegalArgumentException();
        }
    }

    public ImmutableRGB(int red, int blue, int green, String name) {
        check(red, green, blue);
        this.red = red;
        this.blue = blue;
        this.green = green;
        this.name = name;
    }

    public int getRGB() {
        return ((red << 16) | (green << 8) | blue);
    }

    public String getName() {
        return name;
    }

    public ImmutableRGB invert() {
        return new ImmutableRGB(
                255 - red, 255 - blue, 255 - green,
                "Inverse of" + name
        );
    }
}
