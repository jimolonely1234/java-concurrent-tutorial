package com.jimo.executor;

import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * @author jimo
 * @date 2018/10/16 14:11
 */
public class ScheduledExecutorDemo {

	public static void main(String[] args) {
		System.out.println("now: " + new Date());
		final ScheduledExecutorService pool = Executors.newScheduledThreadPool(3);

		// 一次性任务，隔3秒执行
		pool.schedule(new DateTask("task1"), 3, TimeUnit.SECONDS);

		// 周期性：2+3*n秒执行，不会管任务是否执行完
		pool.scheduleAtFixedRate(new DateTask("task2"), 2, 3, TimeUnit.SECONDS);

		// 周期性: 2+3*n,上一次执行完到下一次开始之间等待3秒
		final ScheduledFuture<?> task3 =
				pool.scheduleWithFixedDelay(new DateTask("task3"), 2, 3, TimeUnit.SECONDS);

		// 在10秒后关闭
		pool.schedule(() -> {
			task3.cancel(true);
			pool.shutdown();
		}, 10, TimeUnit.SECONDS);
	}

	private static class DateTask implements Runnable {

		private String name;

		DateTask(String name) {
			this.name = name;
		}

		@Override
		public void run() {
			System.out.println(name + ": " + new Date());
			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (InterruptedException e) {
			}
		}
	}
}
