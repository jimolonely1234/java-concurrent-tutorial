package com.jimo.executor;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.Executor;

/**
 * 序列执行任务,接受一个Executor来执行
 *
 * @author jimo
 * @date 2018/10/10 8:40
 */
public class SerialExecutor implements Executor {
	final private Queue<Runnable> tasks = new ArrayDeque<>();
	final Executor executor;
	private Runnable current;

	public SerialExecutor(Executor executor) {
		this.executor = executor;
	}

	@Override
	public synchronized void execute(Runnable r) {

		// new 一个Runnable，在里面直接执行新的任务,然后执行下一个
		// 这一个新的Runnable追加到队列里，并没立即执行
		tasks.offer(() -> {
			try {
				r.run();
			} finally {
				scheduleNext();
			}
		});

		// 只有上一个任务执行完了，current才为null
		if (current == null) {

			// 直到这里才出列被executor执行
			scheduleNext();
		}
	}

	protected synchronized void scheduleNext() {
		if ((current = tasks.poll()) != null) {
			executor.execute(current);
		}
	}
}
