package com.jimo.executor;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 接受网络请求处理服务
 *
 * @author jimo
 * @date 2018/10/11 10:18
 */
public class NetworkService implements Runnable {

	private final ServerSocket serverSocket;
	private final ExecutorService pool;

	public NetworkService(int port, int poolSize) throws IOException {
		serverSocket = new ServerSocket(port);

		// 一般需要限制处理请求的线程数
		pool = Executors.newFixedThreadPool(poolSize);
	}

	@Override
	public void run() {
		try {
			while (true) {
				pool.execute(new Handler(serverSocket.accept()));
			}
		} catch (IOException e) {
			pool.shutdown();
		}
	}

	private class Handler implements Runnable {

		private final Socket socket;

		private Handler(Socket socket) {
			this.socket = socket;
		}

		@Override
		public void run() {
			//TODO 接受网络请求并处理
		}
	}
}
