package com.jimo.executor;

import java.util.concurrent.Executor;

/**
 * @author jimo
 * @date 2018/10/10 8:29
 */
public class ThreadPerTaskExecutor implements Executor {
	@Override
	public void execute(Runnable command) {
		new Thread(command).start();
	}
}
