package com.jimo.executor;

import java.util.concurrent.Executor;

/**
 * 一个最简单的直接执行的同步Executor
 *
 * @author jimo
 * @date 2018/10/10 8:25
 */
public class DirectExecutor implements Executor {

	@Override
	public void execute(Runnable command) {
		command.run();
	}
}
