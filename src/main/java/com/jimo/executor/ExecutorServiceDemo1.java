package com.jimo.executor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * 测试控制线程的中断
 *
 * @author jimo
 * @date 2018/10/11 8:28
 */
public class ExecutorServiceDemo1 {

	public static void main(String[] args) {
		final ExecutorService executor = Executors.newSingleThreadExecutor();
		executor.submit(new Task());
		System.out.println("关闭executor");
		executor.shutdown();
		try {
			// 等待5秒再跳到finally
			executor.awaitTermination(5, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			System.out.println("任务被中断");
		} finally {
			if (!executor.isTerminated()) {
				System.out.println("取消未完成的任务");

				// 立即关闭，不管当前正在执行的任务
				executor.shutdownNow();
			}

			System.out.println("关闭完成");
		}
	}

	private static class Task implements Runnable {

		@Override
		public void run() {
			final int duration = 7;
			try {
				System.out.println("我是一只在跑的小白兔...");
				TimeUnit.SECONDS.sleep(duration);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
