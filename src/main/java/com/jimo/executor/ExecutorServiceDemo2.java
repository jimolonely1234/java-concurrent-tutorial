package com.jimo.executor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author jimo
 * @date 2018/10/11 9:30
 */
public class ExecutorServiceDemo2 {

	public static void main(String[] args) {
		final ExecutorService executor = Executors.newSingleThreadExecutor();
		executor.submit(new Task());
		shutdownAndAwaitTermination(executor);
	}

	private static void shutdownAndAwaitTermination(ExecutorService pool) {

		// 拒绝接受新的任务提交，但会等待之前的任务完成
		pool.shutdown();

		try {
			final int timeout = 3;
			// 等待3秒直到executor执行完，如果3秒后没执行完返回false
			if (!pool.awaitTermination(timeout, TimeUnit.SECONDS)) {

				// 立即关闭，取消当前正在执行的任务
				// 但是并不保证一定会终止线程，所以有下面的等待过程
				pool.shutdownNow();
				System.out.println("第一次中断");

				// 等待强制关闭后线程的结束,算是一个回复
				if (!pool.awaitTermination(timeout, TimeUnit.SECONDS)) {
					System.err.println("executor没有终止掉");
				}
			}
		} catch (InterruptedException e) {
			System.err.println("2次都没杀死");

			// 如果2次等待都没有终止，那么会抛出中断异常，所以下面再进行关闭
			pool.shutdownNow();

			// 保持中断状态
			Thread.currentThread().interrupt();
		}
	}

	private static class Task implements Runnable {

		@Override
		public void run() {
			final int duration = 4;
			try {
				System.out.println("我是一只在追小白兔的大灰狼...");
				TimeUnit.SECONDS.sleep(duration);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
