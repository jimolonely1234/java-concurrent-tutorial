package com.jimo.highlevelapi;

import java.util.concurrent.RecursiveAction;

public class ForkJoinBlur extends RecursiveAction {
    private int[] sourceImg;
    private int[] destImg;
    private int start;
    private int length;

    /**
     * 模糊窗口，应该是奇数
     */
    private int blurWidth = 15;

    public ForkJoinBlur(int[] sourceImg, int[] destImg, int start, int length) {
        this.sourceImg = sourceImg;
        this.destImg = destImg;
        this.start = start;
        this.length = length;
    }

    protected void computeDirectly() {
        int sidePixels = (blurWidth - 1) / 2;
        for (int i = start; i < start + length; i++) {
            float rt = 0, gt = 0, bt = 0;
            for (int mi = -sidePixels; mi <= sidePixels; mi++) {
                int mindex = Math.min(Math.max(mi + i, 0), sourceImg.length - 1);
                int pixel = sourceImg[mindex];
                rt += (float) ((pixel & 0x00ff0000) >> 16) / blurWidth;
                gt += (float) ((pixel & 0x0000ff00) >> 16) / blurWidth;
                bt += (float) ((pixel & 0x000000ff) >> 16) / blurWidth;
            }
            int dpixel = 0xff000000 | ((int) rt << 16) | ((int) gt << 8) | ((int) bt);
            destImg[i] = dpixel;
        }
    }

    protected static int threshHold = 100000;

    protected void compute() {
        if (length < threshHold) {
            computeDirectly();
            return;
        }
        int split = length / 2;
        this.invokeAll(new ForkJoinBlur(sourceImg, destImg, start, split),
                new ForkJoinBlur(sourceImg, destImg, start + split, length - split));
    }

    public static void main(String[]args){
//        Arrays.parallelSort();
    }

}
