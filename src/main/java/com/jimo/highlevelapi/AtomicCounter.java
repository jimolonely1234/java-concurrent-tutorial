package com.jimo.highlevelapi;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by ThinkPad on 2018/8/20.
 */
public class AtomicCounter {
    private AtomicInteger c = new AtomicInteger(0);

    public void increment() {
        c.incrementAndGet();
    }

    public void decrement() {
        c.decrementAndGet();
    }

    public int getValue() {
        return c.get();
    }
}
