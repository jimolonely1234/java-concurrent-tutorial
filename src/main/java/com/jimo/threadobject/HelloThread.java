package com.jimo.threadobject;

/**
 * Created by ThinkPad on 2018/8/8.
 */
public class HelloThread extends Thread {

    @Override
    public void run() {
        System.out.println("hello thread");
    }

    public static void main(String[] args) {
        new HelloThread().start();
    }
}
