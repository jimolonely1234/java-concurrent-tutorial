package com.jimo.threadobject;

/**
 * Created by ThinkPad on 2018/8/8.
 */
public class HelloRunnable implements Runnable {

    public void run() {
        System.out.println("hello runnable");
    }

    public static void main(String[] args) {
        (new Thread(new HelloRunnable())).start();
    }
}
