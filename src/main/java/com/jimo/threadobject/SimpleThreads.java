package com.jimo.threadobject;

/**
 * Created by ThinkPad on 2018/8/8.
 */
public class SimpleThreads {

    static void showThreadMessage(String message) {
        System.out.format("%s: %s%n", Thread.currentThread().getName(), message);
    }

    private static class MessageLoop implements Runnable {

        public void run() {
            String[] infos = {"jimo", "hehe", "haha", "momo"};
            for (String info : infos) {
                try {
                    Thread.sleep(3000);
                    showThreadMessage(info);
                } catch (InterruptedException e) {
                    showThreadMessage("我还没有死,只是被中断了");
                }
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        long patience = 1000 * 3;//最多等待

        System.out.println("开启MessageLoop线程...");
        long startTime = System.currentTimeMillis();
        Thread msgThread = new Thread(new MessageLoop());
        msgThread.start();

        showThreadMessage("等待MessageLoop线程执行完...");

        while (msgThread.isAlive()) {
            showThreadMessage("Still Waiting...");
            msgThread.join(1000);//主线程最多等待1秒
            if ((System.currentTimeMillis() - startTime) > patience && msgThread.isAlive()) {
                showThreadMessage("等烦了，中断你!");
                msgThread.interrupt();
                msgThread.join();// 虽然中断了，但还是要执行完
            }
        }

        showThreadMessage("Finally!");
    }
    /*
    正常时：
    开启MessageLoop线程...
    main: 等待MessageLoop线程执行完...
    main: Still Waiting...
    main: Still Waiting...
    main: Still Waiting...
    Thread-0: jimo
    main: Still Waiting...
    main: Still Waiting...
    main: Still Waiting...
    Thread-0: hehe
    main: Still Waiting...
    main: Still Waiting...
    main: Still Waiting...
    Thread-0: haha
    main: Still Waiting...
    main: Still Waiting...
    main: Still Waiting...
    Thread-0: momo
    main: Finally!

    改为等待6秒：
    开启MessageLoop线程...
    main: 等待MessageLoop线程执行完...
    main: Still Waiting...
    main: Still Waiting...
    main: Still Waiting...
    Thread-0: jimo
    main: Still Waiting...
    main: Still Waiting...
    main: Still Waiting...
    Thread-0: hehe
    main: 等烦了，中断你!
    Thread-0: 我还没有死,只是被中断了
    Thread-0: momo
    main: Finally!
     */
}
