package com.jimo.threadobject;

/**
 * Created by ThinkPad on 2018/8/8.
 */
public class SleepMessages {
    public static void main(String[] args) {
        String[] infos = {"ab", "abc", "abcd", "abcde"};

        for (String s : infos) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                //我被中断了
                return;
            }
            System.out.println(s);
        }
    }

    public static void test() throws InterruptedException {
        String[] infos = {"ab", "abc", "abcd", "abcde"};
        for (String s : infos) {
            doSomthing(s);
            if (Thread.interrupted()) {
//                return;//被中断了，直接返回
                throw new InterruptedException();
            }
        }
    }

    private static void doSomthing(String s) {
    }
}
