package com.jimo.threadobject;

/**
 * Created by ThinkPad on 2018/8/8.
 */
public class JoinThread {
    public static void main(String[] args) throws InterruptedException {
        HelloThread helloThread = new HelloThread();

        System.out.println("hello begin");

        helloThread.start();
        helloThread.join(); //main线程等待hello线程执行完才执行

        System.out.println("hello end");
    }
}
