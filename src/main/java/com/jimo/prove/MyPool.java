package com.jimo.prove;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Semaphore;

/**
 * @author jimo
 * @date 2018/9/13 17:44
 * @code 使用Semaphore实现一个对象存储池，可以借出对象和回收对象，使用对象池的类必须自己归还对象，
 * 否则达到最大容量将不能借出对象, 所以一般这个pool的使用者是我们自己的程序
 */
public class MyPool {
    private final int MAX_AVAILABLE;
    private final Semaphore available;
    protected final MyGreatObject[] items;
    protected final boolean[] used;

    /**
     * @author jimo
     * @date 2018/9/13 18:02
     * @code 我的私有资源对象
     */
    class MyGreatObject {
        private String name;

        public MyGreatObject(String name) {
            this.name = name;
        }
    }

    public MyPool(int maxSize) {
        this.MAX_AVAILABLE = maxSize;
        available = new Semaphore(MAX_AVAILABLE, true);
        items = new MyGreatObject[MAX_AVAILABLE];
        used = new boolean[MAX_AVAILABLE];

        /*初始化一些对象*/
        for (int i = 0; i < MAX_AVAILABLE; i++) {
            items[i] = new MyGreatObject("对象" + i);
        }
    }

    public MyGreatObject getItem() throws InterruptedException {
        available.acquire();
        return getNextAvailableItem();
    }

    public void returnBack(MyGreatObject obj) {
        if (markAsUnused(obj)) {
            System.out.println("归还对象：" + obj.name);
            available.release();
        }
    }

    private synchronized boolean markAsUnused(MyGreatObject obj) {
        for (int i = 0; i < MAX_AVAILABLE; i++) {
            if (obj == items[i]) {
                if (used[i]) {
                    used[i] = false;
                    return true;
                } else {
                    /*如果没被使用，那么你归还的就是不合法的*/
                    return false;
                }
            }
        }
        return false;
    }

    private synchronized MyGreatObject getNextAvailableItem() {
        for (int i = 0; i < MAX_AVAILABLE; i++) {
            if (!used[i]) {
                used[i] = true;
                System.out.println("借出对象：" + items[i].name);
                return items[i];
            }
        }
        return null;
    }

    public static void main(String[] args) {
        final MyPool pool = new MyPool(5);
        final int cnt = 20;
        List<MyGreatObject> objects = new ArrayList<>(cnt);
        final Random random = new Random();
        for (int i = 0; i < cnt; i++) {
            new Thread(() -> {
                try {
                    final MyGreatObject item = pool.getItem();
                    objects.add(item);
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                }
            }).start();

            new Thread(() -> {
                final int index = random.nextInt(cnt);
                if(index<objects.size()) {
                    pool.returnBack(objects.get(index));
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {

                }
            }).start();
        }
    }
}
