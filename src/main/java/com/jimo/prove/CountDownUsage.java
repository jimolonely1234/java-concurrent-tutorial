package com.jimo.prove;

import java.util.concurrent.CountDownLatch;

/**
 * @author jimo
 * @code CountDownLatch用法测试
 * @date 2018/9/10 11:33
 */
public class CountDownUsage {

    public static void main(String[] args) {

    }

    /**
     * @code 记录线程times次的运行时间
     * @author jimo
     * @date 2018/9/10 11:40
     */
    public static long timeCost(final int times, final Runnable task) throws InterruptedException {
        if (times < 0) {
            throw new IllegalArgumentException();
        }
        final CountDownLatch startLatch = new CountDownLatch(1);
        final CountDownLatch overLatch = new CountDownLatch(times);
        for (int i = 0; i < times; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        startLatch.await();

                        task.run();
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                        e.printStackTrace();
                    } finally {
                        overLatch.countDown();
                    }
                }
            }).start();
        }
        final long startTime = System.nanoTime();
        startLatch.countDown();
        overLatch.await();
        return System.nanoTime() - startTime;
    }
}
