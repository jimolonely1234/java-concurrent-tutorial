package com.jimo.prove;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * @author jimo
 * @date 2018/9/13 16:55
 * @code cyclic barrier demo
 */
public class Solver {
    final int N;
    final float[][] data;
    final CyclicBarrier barrier;
    private boolean done;

    class Worker implements Runnable {
        int myRow;

        public Worker(int myRow) {
            this.myRow = myRow;
        }

        @Override
        public void run() {
            while (!done) {
                processRow(myRow);
                try {
                    final int index = barrier.await();
                    System.out.println("index now is: " + index);
                } catch (InterruptedException | BrokenBarrierException e) {
                    return;
                }
            }
        }

        private void processRow(int myRow) {
            System.out.println("处理第" + myRow + "行....");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                return;
            }
        }
    }

    private void mergeRows() {
        System.out.println("执行合并操作");
    }

    public Solver(float[][] data) throws InterruptedException {
        this.data = data;
        N = data.length;
        /*在所有其他线程执行完了才执行合并行*/
        final Runnable barrierAction = this::mergeRows;
        barrier = new CyclicBarrier(N, barrierAction);

        List<Thread> threads = new ArrayList<>(N);
        for (int i = 0; i < N; i++) {
            final Thread thread = new Thread(new Worker(i));
            threads.add(thread);
            thread.start();
        }
        /*等待执行结束*/
        for (Thread thread : threads) {
            thread.join();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        int row = 10;
        float[][] data = new float[row][];
        for (int i = 0; i < row; i++) {
            data[i] = new float[row];
            for (int j = 0; j < row; j++) {
                data[i][j] = 1;
            }
        }
        new Solver(data);
    }
}
