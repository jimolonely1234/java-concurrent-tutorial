package com.jimo.prove;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * @author jimo
 * @date 2018/9/13 16:28
 * @code demo
 */
public class CyclicBarrierDemo {
    final CyclicBarrier barrier;

    final int MAX_TASK;

    public CyclicBarrierDemo(int maxTask) {
        MAX_TASK = maxTask;
        barrier = new CyclicBarrier(maxTask + 1);
    }

    public void doWork(final Runnable work) {
        new Thread(() -> {
            work.run();
            try {
                final int index = barrier.await();
                doWithIndex(index);
            } catch (InterruptedException e) {
                return;
            } catch (BrokenBarrierException e) {
                return;
            }

        }).start();
    }

    private void doWithIndex(int index) {
        if (index == MAX_TASK / 3) {
            System.out.println("left 30%");
        } else if (index == MAX_TASK / 2) {
            System.out.println("left 50%");
        } else if (index == 0) {
            System.out.println("run over");
        }
    }

    public void waitForNext() {
        try {
            doWithIndex(barrier.await());
        } catch (InterruptedException e) {
            return;
        } catch (BrokenBarrierException e) {
            return;
        }
    }

    public static void main(String[] args) {
        /*每组10个，总共100个，10组,一组一组按顺序来执行*/
        final int count = 10;
        final CyclicBarrierDemo barrierDemo = new CyclicBarrierDemo(count);
        for (int i = 0; i < 100; i++) {
            barrierDemo.doWork(() -> {
                /*doSomething*/
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    return;
                }
            });
            if ((i + 1) % count == 0) {
                /*一组执行完了*/
                barrierDemo.waitForNext();
            }
        }
    }
}
