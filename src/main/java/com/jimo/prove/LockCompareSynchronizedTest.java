package com.jimo.prove;

import org.junit.Test;

import java.util.concurrent.TimeUnit;

/**
 * @author wangpeng
 * @func Lock和Synchronized性能比较
 * @date 2018/8/29 10:02
 */
public class LockCompareSynchronizedTest {

    private int loopCount = 10000000;
    private int threadCount = 10;

    /**
     * @func
     * @author wangpeng
     * @date 2018/8/29 10:04
     */
    @Test
    public void testLock() throws InterruptedException {
        final long startTime = System.nanoTime();
        final Thread[] threads = new Thread[threadCount];
        final AtomicIntegerWithLock lock = new AtomicIntegerWithLock(0);
        for (int i = 0; i < threadCount; i++) {
            threads[i] = new Thread(() -> {
                for (int j = 0; j < loopCount; j++) {
                    lock.incrementAndGet();
                }
            });
        }
        for (Thread thread : threads) {
            thread.start();
        }
        for (Thread thread : threads) {
            thread.join();
        }
        final long endTime = System.nanoTime();
        System.out.println(TimeUnit.NANOSECONDS.toMillis(endTime - startTime) + "ms");
        /*2976ms*/
    }

    private static int value = 0;

    @Test
    public void testSynchronized() throws InterruptedException {
        final long startTime = System.nanoTime();
        final Object lock = new Object();
        final Thread[] threads = new Thread[threadCount];
        for (int i = 0; i < threadCount; i++) {
            threads[i] = new Thread(() -> {
                for (int j = 0; j < loopCount; j++) {
                    synchronized (lock) {
                        ++value;
                    }
                }
            });

        }
        for (Thread thread : threads) {
            thread.start();
        }
        for (Thread thread : threads) {
            thread.join();
        }
        final long endTime = System.nanoTime();
        System.out.println(TimeUnit.NANOSECONDS.toMillis(endTime - startTime) + "ms");
        /*3458ms*/
    }
}
