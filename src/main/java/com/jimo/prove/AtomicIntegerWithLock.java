package com.jimo.prove;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author wangpeng
 * @func 使用Lock实现AtomicInteger
 * @date 2018/8/29 9:53
 */
public class AtomicIntegerWithLock {
    private int value;
    private Lock lock = new ReentrantLock();

    public AtomicIntegerWithLock(int value) {
        this.value = value;
    }

    public final int get() {
        lock.lock();
        try {
            return value;
        } finally {
            lock.unlock();
        }
    }

    public final void set(int value) {
        lock.lock();
        try {
            this.value = value;
        } finally {
            lock.unlock();
        }
    }

    public final boolean compareAndGet(int expect, int update) {
        lock.lock();
        try {
            if (expect == value) {
                value = update;
                return true;
            }
            return false;
        } finally {
            lock.unlock();
        }
    }

    public final int incrementAndGet() {
        lock.lock();
        try {
            return ++value;
        } finally {
            lock.unlock();
        }
    }
}
