package com.jimo.prove;

import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author jimo
 * @date 2018/9/14 15:47
 * @code 读写锁例子
 */
public class CacheData {
    /**
     * 用来缓存数据
     */
    Object data;
    volatile boolean cacheValid;
    final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();

    void processData() {
        rwl.readLock().lock();
        if (!cacheValid) {
            /*在获取写锁前必须释放读锁*/
            rwl.readLock().unlock();
            rwl.writeLock().lock();
            try {
                /*再次检查，因为另外的线程可能获取写锁改变了状态*/
                if (!cacheValid) {
                    /*data = ...;*/
                    cacheValid = true;
                }
                /*在释放写锁之前通过获取读锁达到降级的目的*/
                rwl.readLock().lock();
            } finally {
                /*释放了写锁，但依然持有读锁*/
                rwl.writeLock().unlock();
            }
        }
        try {
            use(data);
        } finally {
            rwl.readLock().unlock();
        }
    }

    private void use(Object data) {
    }
}
