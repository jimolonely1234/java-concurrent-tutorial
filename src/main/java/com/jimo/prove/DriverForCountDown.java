package com.jimo.prove;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledThreadPoolExecutor;

/**
 * @author jimo
 * @code 使用CountDownLatch
 * @date 2018/9/10 12:25
 */
public class DriverForCountDown {

    private static final int N = 2;

    public static void main(String[] args) throws InterruptedException {
        final CountDownLatch downSignal = new CountDownLatch(N);
        Executor executor = new ScheduledThreadPoolExecutor(N);

        for (int i = 0; i < N; i++) {
            executor.execute(new WorkRunnbale(downSignal, i));
        }
        downSignal.await(); /*等待任务完成*/
    }

    static class WorkRunnbale implements Runnable {

        private final CountDownLatch downSignal;
        private final int i;

        WorkRunnbale(CountDownLatch downSignal, int i) {
            this.downSignal = downSignal;
            this.i = i;
        }

        @Override
        public void run() {
            doWork();
            downSignal.countDown(); /*每完成一个线程，就减少计数*/
        }

        private void doWork() {
        }
    }
}
